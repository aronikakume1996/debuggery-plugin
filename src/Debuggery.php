<?php

namespace Debuggery;

use Debuggery\Admin\Admin;

/**
 * The Debuggery class
 */
class Debuggery
{
  /**
   * @var Debuggery
   */
  private static $instance = null;
  protected $admin;

  /**
   * Debuggery constructor
   */
  private function __construct()
  {
    $this->admin = new Admin();
  }
  /**
   * Get Debuggery singleton instance
   * @return Debuggery
   */
  public static function  instance()
  {
    if (is_null(static::$instance)) {
      static::$instance = new static();
    }
    return static::$instance;
  }

  /** 
   * Initialize Debuggery singleton
   * 
   * @return Debuggery
   */
  public static function init()
  {
    return static::instance();
  }
}
