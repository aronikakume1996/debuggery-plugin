<?php

namespace Debuggery\Admin;

abstract class Page
{
  public function __construct()
  {
    add_action('admin_menu', array($this, 'register'), 999);
  }

  /**
   * Register the admin page
   */
  abstract public function register();
}
