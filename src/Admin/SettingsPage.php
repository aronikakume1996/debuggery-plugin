<?php

namespace Debuggery\Admin;

class SettingsPage extends Page
{
  // public function __construct()
  // {
  //   add_action('admin_menu', array($this, 'register_submenu_pages'), 999);
  // }
  /**
   * Register our settings page.
   */
  public function register()
  {
    add_options_page(
      'Debuggery Settings', // page title
      'Debuggery', // menu title
      'manage_options', // capability
      'debuggery', // menu slug
      array($this, 'create_settings_page'), // callable for admin page output
      99 // position in the menu order
    );
  }

  public function create_settings_page()
  {
?>
    <div class="debuggery-admin wrap">
      <h1>Debuggery Settings</h1>
      <p>Additional debugging tools for development purposes.</p>
    </div>
<?php
  }
}
