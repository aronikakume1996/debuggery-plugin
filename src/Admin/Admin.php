<?php

namespace Debuggery\Admin;

/**
 * The Debuggery class
 */
class Admin
{
  /**
   * @var array
   */
  protected $page_classes = array(
    SettingsPage::class,
  );

  /**
   * @var array
   */
  protected $pages = array();

  public function __construct()
  {
    $this->init_pages();
  }

  protected function init_pages()
  {
    $admin_pages = apply_filters(
      'debuggery_init_admin_pages',
      $this->page_classes
    );

    foreach ($admin_pages as $admin_page) {
      $page = new $admin_page();
      $this->pages[$page->name] = $page;
    }
  }
}
