<?php

/**
 * Plugin Name: Debuggery
 * Plugin URI:  http://docs.debuggery.com/api/debuggery.html
 * Description: A Collection of debug helpers.
 * Author: Mebratu
 * Author URI: http://www.mebratu.com
 * Version: 1.0.0
 * License: GPL v2 or later
 * Text Domain: debuggery
 */

use Debuggery\Debuggery;

// if (!defined('APSPATH')) {
//   exit;
// }

// Autoload dependencies
require_once __DIR__ . '/vendor/autoload.php';

if (class_exists(Debuggery::class)) {
  // $debuggery = new \Debuggery\Debuggery();
  Debuggery::init();
}
